import sys
sys.path.insert(0, 'quantbacktest/components/')
sys.path.insert(0, 'quantbacktest/')

from _1_data_preparation import load_data
from _2_strategy_execution import execute_order
from _helper_functions import find_price

# For managing dates
from datetime import datetime

# For allowing for flexible time differences (frequencies)
from pandas.tseries.offsets import Timedelta

# For managing tables properly
from pandas import read_csv

# For converting strings to dict
import json

# For path compatbility on all platforms
from pathlib import Path


def test_unittest_execute_order():
    base_path = Path(__file__).parent.parent

    dict_strategy_hyperparameters = {
        'int_maximum_deviation_in_days': 0,
        'string_prices_table_id_column_name': 'token_itin',
        'boolean_allow_partially_filled_orders': True
    }

    df_prices = load_data(
        string_file_path_with_price_data=base_path/'quantbacktest/assets/raw_itsa_data/btc-usd-max-coingecko.csv',
        dict_strategy_hyperparameters=dict_strategy_hyperparameters,
        string_file_path_with_token_data=None,
        string_name_of_foreign_key_in_price_data_table=None,
        string_name_of_foreign_key_in_token_metadata_table=None
    )

    dict_fees={
        'float_absolute_fee_buy_order': 0.0,
        'float_absolute_fee_sell_order': 0.0,
        'float_percentage_buying_fees_and_spread': 0.02,
        'float_percentage_selling_fees_and_spread': 0.02
    }

    float_price = find_price(
        df_prices=df_prices,
        desired_index=(datetime(2019, 4, 15), 'TP3B-248N-Q'),
        boolean_allow_older_prices=False,
        boolean_allow_newer_prices=False,
        boolean_warnings=False,
        boolean_errors=False,
        timedelta_to_allow_to_go_back=Timedelta(days=0),
        timedelta_to_allow_to_go_to_the_future=Timedelta(days=0)
    )

    dict_display_options = {
        'boolean_plot_heatmap': False,
        'boolean_test': False,  # If multi-asset strategy is used, this will cause sampling of the signals to speed up the run for testing during development.
        'boolean_warning_no_price_for_last_day': False,
        'boolean_warning_no_price_during_execution': False,
        'boolean_warning_no_price_for_intermediate_valuation': True,
        'boolean_warning_alternative_date': False,
        'boolean_warning_calculate_daily_returns_alternative_date': False,
        'boolean_warning_no_price_for_calculate_daily_returns': False,
        'boolean_warning_buy_order_could_not_be_filled': True,
        'boolean_warning_sell_order_could_not_be_filled': True,
        'boolean_errors_on_benchmark_gap': True,
        'boolean_plot_equity_curve': False,
        'boolean_save_equity_curve_to_disk': True,
        'string_results_directory': '/home/janspoerer/code/janspoerer/quantbacktest/tests/test_results/',
        'boolean_percentage_in_signal_table': False
    }

    dict_constraints = {
        'float_maximum_individual_asset_exposure_all': 1.0,  # Not yet implemented
        'dict_maximum_individual_asset_exposure_individual': {},  # Not yet implemented
        'float_maximum_gross_exposure': 1.0,  # Already implemented
        'boolean_allow_shortselling': False,  # Shortselling not yet implemented
        'float_minimum_cash': 100.0,
    }

    dict_general_settings = {
        'int_rounding_decimal_places': 4,
        'int_rounding_decimal_places_for_security_quantities': 0,
    }

    # Testing a valid buy order
    df_trading_journal = read_csv(
        base_path/'tests/test_data/trading_journal_valid_buy_order.csv',
        sep=',',
        parse_dates=['datetime'],
        infer_datetime_format=True
    )
    df_trading_journal['Dict of assets in portfolio'] = df_trading_journal['Dict of assets in portfolio'].apply(
        lambda x: json.loads(x.replace("'", "\""))
    )
    print(type(df_trading_journal['Dict of assets in portfolio'].iloc[0]))
    print(df_trading_journal['Dict of assets in portfolio'])
    print(df_trading_journal['Dict of assets in portfolio'])
    print(df_trading_journal['Dict of assets in portfolio'])
    print(df_trading_journal['Dict of assets in portfolio'])
    execute_order(
        boolean_buy=True,
        datetime_datetime=datetime(2019, 4, 15),
        string_crypto_key='TP3B-248N-Q',
        float_number_to_be_bought=1,
        df_prices=df_prices,
        df_trading_journal=df_trading_journal,
        float_margin_loan_rate=100000000.0,
        dict_fees=dict_fees,
        float_budget_in_usd=100000,
        float_price=float_price,
        dict_display_options=dict_display_options,
        dict_constraints=dict_constraints,
        dict_general_settings=dict_general_settings,
        boolean_allow_partially_filled_orders=False
    )

    # Testing a valid sell order
    df_trading_journal = read_csv(
        base_path/'tests/test_data/trading_journal_valid_sell_order.csv',
        sep=',',
        parse_dates=['datetime'],
        infer_datetime_format=True
    )
    df_trading_journal['Dict of assets in portfolio'] = df_trading_journal['Dict of assets in portfolio'].apply(
        lambda x: json.loads(x.replace("'", "\""))
    )
    execute_order(
        boolean_buy=False,
        datetime_datetime=datetime(2019, 4, 15),
        string_crypto_key='TP3B-248N-Q',
        float_number_to_be_bought=-1,
        df_prices=df_prices,
        df_trading_journal=df_trading_journal,
        float_margin_loan_rate=100000.0,
        dict_fees=dict_fees,
        float_budget_in_usd=0,
        float_price=float_price,
        dict_display_options=dict_display_options,
        dict_constraints=dict_constraints,
        dict_general_settings=dict_general_settings,
        boolean_allow_partially_filled_orders=False
    )

    # Testing an invalid empty order
    df_trading_journal = read_csv(
        base_path/'tests/test_data/trading_journal_invalid_empty_order.csv',
        sep=',',
        parse_dates=['datetime'],
        infer_datetime_format=True
    )
    df_trading_journal['Dict of assets in portfolio'] = df_trading_journal['Dict of assets in portfolio'].apply(
        lambda x: json.loads(x.replace("'", "\""))
    )
    try:
        execute_order(
            boolean_buy=None,
            datetime_datetime=datetime(2019, 4, 15),
            string_crypto_key='TP3B-248N-Q',
            float_number_to_be_bought=0,
            df_prices=df_prices,
            df_trading_journal=df_trading_journal,
            float_margin_loan_rate=100000.0,
            dict_fees=dict_fees,
            float_budget_in_usd=0,
            float_price=float_price,
            dict_display_options=dict_display_options,
            dict_constraints=dict_constraints,
            dict_general_settings=dict_general_settings,
            boolean_allow_partially_filled_orders=False
        )
    except ValueError:
        pass
    except:
        ValueError('Expected a ValueError')

    # Testing an buy order that cannot be filled (with partial filling prohibited)
    df_trading_journal = read_csv(
        base_path/'tests/test_data/trading_journal_invalid_buy_order_cannot_be_filled.csv',
        sep=',',
        parse_dates=['datetime'],
        infer_datetime_format=True
    )
    df_trading_journal['Dict of assets in portfolio'] = df_trading_journal['Dict of assets in portfolio'].apply(
        lambda x: json.loads(x.replace("'", "\""))
    )
    try:
        execute_order(
            boolean_buy=True,
            datetime_datetime=datetime(2019, 4, 15),
            string_crypto_key='TP3B-248N-Q',
            float_number_to_be_bought=100000000.0,
            df_prices=df_prices,
            df_trading_journal=df_trading_journal,
            float_margin_loan_rate=1000000,
            dict_fees=dict_fees,
            float_budget_in_usd=100000,
            float_price=float_price,
            dict_display_options=dict_display_options,
            dict_constraints=dict_constraints,
            dict_general_settings=dict_general_settings,
            boolean_allow_partially_filled_orders=False
        )
    except ValueError:
        pass
    except:
        raise ValueError('Expected a ValueError!')

    # Testing an buy order that cannot be filled (with partial filling allowed)
    df_trading_journal = read_csv(
        base_path/'tests/test_data/trading_journal_valid_buy_order_cannot_be_filled_but_partially.csv',
        sep=',',
        parse_dates=['datetime'],
        infer_datetime_format=True
    )
    df_trading_journal['Dict of assets in portfolio'] = df_trading_journal['Dict of assets in portfolio'].apply(
        lambda x: json.loads(x.replace("'", "\""))
    )
    dict_order = execute_order(
        boolean_buy=True,
        datetime_datetime=datetime(2019, 4, 15),
        string_crypto_key='TP3B-248N-Q',
        float_number_to_be_bought=100000000.0,
        df_prices=df_prices,
        df_trading_journal=df_trading_journal,
        float_margin_loan_rate=1000000,
        dict_fees=dict_fees,
        float_budget_in_usd=0,
        float_price=float_price,
        dict_display_options=dict_display_options,
        dict_constraints=dict_constraints,
        dict_general_settings=dict_general_settings,
        boolean_allow_partially_filled_orders=True
    )
    assert dict_order['Number bought'] == 19.0

    # Testing a sell order that cannot be filled (with partial filling prohibited)
    df_trading_journal = read_csv(
        base_path/'tests/test_data/trading_journal_invalid_sell_order_cannot_be_filled.csv',
        sep=',',
        parse_dates=['datetime'],
        infer_datetime_format=True
    )
    df_trading_journal['Dict of assets in portfolio'] = df_trading_journal['Dict of assets in portfolio'].apply(
        lambda x: json.loads(x.replace("'", "\""))
    )
    try:
        execute_order(
            boolean_buy=False,
            datetime_datetime=datetime(2019, 4, 15),
            string_crypto_key='TP3B-248N-Q',
            float_number_to_be_bought=-100000000.0,
            df_prices=df_prices,
            df_trading_journal=df_trading_journal,
            float_margin_loan_rate=1000000,
            dict_fees=dict_fees,
            float_budget_in_usd=0,
            float_price=float_price,
            dict_display_options=dict_display_options,
            dict_constraints=dict_constraints,
            dict_general_settings=dict_general_settings,
            boolean_allow_partially_filled_orders=False
        )
        raise ValueError('This test should have failed!')
    except ValueError:
        pass
    except:
        raise ValueError('A ValueError was expected!')

    # Testing a sell order that cannot be filled (with partial filling allowed)
    df_trading_journal = read_csv(
        base_path/'tests/test_data/trading_journal_valid_sell_order_cannot_be_filled_but_partially.csv',
        sep=',',
        parse_dates=['datetime'],
        infer_datetime_format=True
    )
    df_trading_journal['Dict of assets in portfolio'] = df_trading_journal['Dict of assets in portfolio'].apply(
        lambda x: json.loads(x.replace("'", "\""))
    )
    dict_order = execute_order(
        boolean_buy=False,
        datetime_datetime=datetime(2019, 4, 15),
        string_crypto_key='TP3B-248N-Q',
        float_number_to_be_bought=-100000000.0,
        df_prices=df_prices,
        df_trading_journal=df_trading_journal,
        float_margin_loan_rate=1000000,
        dict_fees=dict_fees,
        float_budget_in_usd=0,
        float_price=float_price,
        dict_display_options=dict_display_options,
        dict_constraints=dict_constraints,
        dict_general_settings=dict_general_settings,
        boolean_allow_partially_filled_orders=True
    )
    assert dict_order['Number bought'] == -1

    # Testing an order with incorrect price
    df_trading_journal = read_csv(
        base_path/'tests/test_data/trading_journal_invalid_buy_order_wrong_price.csv',
        sep=',',
        parse_dates=['datetime'],
        infer_datetime_format=True
    )
    df_trading_journal['Dict of assets in portfolio'] = df_trading_journal['Dict of assets in portfolio'].apply(
        lambda x: json.loads(x.replace("'", "\""))
    )
    try:
        execute_order(
            boolean_buy=False,
            datetime_datetime=datetime(2019, 4, 15),
            string_crypto_key='TP3B-248N-Q',
            float_number_to_be_bought=-1,
            df_prices=df_prices,
            df_trading_journal=df_trading_journal,
            float_margin_loan_rate=100000.0,
            dict_fees=dict_fees,
            float_budget_in_usd=0,
            float_price=1,
            dict_display_options=dict_display_options,
            dict_constraints=dict_constraints,
            dict_general_settings=dict_general_settings,
            boolean_allow_partially_filled_orders=False
        )
    except ValueError:
        pass
    except:
        raise ValueError('Expected a ValueError!')


if __name__ == '__main__':
    test_unittest_execute_order()
