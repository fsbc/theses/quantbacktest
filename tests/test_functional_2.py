from quantbacktest import backtest_visualizer

# For managing dates
from datetime import datetime

# For allowing for flexible time differences (frequencies)
from pandas.tseries.offsets import Timedelta


def test_functional_2():
    dict_display_options = {
        'boolean_plot_heatmap': False,
        'boolean_test': False,  # If multi-asset strategy is used, this will cause sampling of the signals to speed up the run for testing during development.
        'boolean_warning_no_price_for_last_day': False,
        'boolean_warning_no_price_during_execution': False,
        'boolean_warning_no_price_for_intermediate_valuation': True,
        'boolean_warning_alternative_date': False,
        'boolean_warning_calculate_daily_returns_alternative_date': False,
        'boolean_warning_no_price_for_calculate_daily_returns': False,
        'boolean_warning_buy_order_could_not_be_filled': True,
        'boolean_warning_sell_order_could_not_be_filled': True,
        'boolean_errors_on_benchmark_gap': True,
        'boolean_plot_equity_curve': False,
        'boolean_save_equity_curve_to_disk': True,
        'string_results_directory': '/home/janspoerer/code/janspoerer/quantbacktest/tests/test_results/'
    }

    dict_general_settings = {
        'int_rounding_decimal_places': 4,
        'int_rounding_decimal_places_for_security_quantities': 0,
    }

    string_excel_worksheet_name = 'weights'

    dict_strategy_hyperparameters = {
        'int_maximum_deviation_in_days': 300,
        'string_prices_table_id_column_name': 'token_itin',
        'string_excel_worksheet_name': string_excel_worksheet_name,  # Set this to None if CSV is used!
        # For OpenMetrics: 9.8
        'list_buy_parameter_space': [0],  # [11, 20] # Times 10! Will be divided by 10.
        # For OpenMetrics: 9.7
        'list_sell_parameter_space': [0],  # [5, 9] # Times 10! Will be divided by 10.
        'float_maximum_relative_exposure_per_buy': 0.34,
        'timedelta_frequency': Timedelta(days=1),
        'int_moving_average_window_in_days': 14,
        'string_id': 'TP3B-248N-Q',
        'boolean_allow_partially_filled_orders': True,
        'boolean_percentage_in_relation_to_total_portfolio_value_in_signal_table': False,
        'boolean_iterative_percentage_approach_signal_table': False,
        'string_file_path_with_signal_data': '/home/janspoerer/code/janspoerer/rubin_schmie/files_from_kirill/2020-05-20_SIM/USDC_BTC/merged_by_jan.csv',
        'boolean_percentage_in_signal_table': True
    }

    dict_constraints = {
        'float_maximum_individual_asset_exposure_all': 1.0,  # Not yet implemented
        'dict_maximum_individual_asset_exposure_individual': {},  # Not yet implemented
        'float_maximum_gross_exposure': 1.0,  # Already implemented
        'boolean_allow_shortselling': False,  # Shortselling not yet implemented
        'float_minimum_cash': 100.0,
    }

    dict_comments = {  # Comments do not change logic, they should be strings.
        'display_options': repr(dict_display_options),
        'strategy_hyperparameters': repr(dict_strategy_hyperparameters)
    }

    backtest_visualizer(
        string_file_path_with_price_data='/home/janspoerer/code/janspoerer/quantbacktest/quantbacktest/assets/raw_itsa_data/20190717_itsa_tokenbase_top600_wtd302_token_daily.csv',
        # ONLY LEAVE THIS LINE UNCOMMENTED IF YOU WANT TO USE ETH-ADDRESSES AS ASSET IDENTIFIERS!
        # file_path_with_token_data='raw_itsa_data/20190717_itsa_tokenbase_top600_wtd301_token.csv',  # Only for multi-asset strategies.
        string_name_of_foreign_key_in_price_data_table='token_itin',
        string_name_of_foreign_key_in_token_metadata_table='token_itin',
        # 1: execute_strategy_white_noise()
        # 2: Not used anymore, can be reassigned
        # 3: execute_strategy_multi_asset() -> Uses strategy table
        # 4: execute_strategy_ma_crossover()
        int_chosen_strategy=3,
        dict_crypto_options={
            'dict_general': {
                'float_percentage_buying_fees_and_spread': 0.005,  # 0.26% is the taker fee for low-volume clients at kraken.com https://www.kraken.com/features/fee-schedule
                'float_percentage_selling_fees_and_spread': 0.005,  # 0.26% is the taker fee for low-volume clients at kraken.com https://www.kraken.com/features/fee-schedule
                # Additional fees may apply for depositing money.
                'float_absolute_fee_buy_order': 0.0,
                'float_absolute_fee_sell_order': 0.0,
            }
        },
        float_budget_in_usd=1000000.00,
        dict_strategy_hyperparameters=dict_strategy_hyperparameters,
        float_margin_loan_rate=0.05,
        list_times_of_split_for_robustness_test=[
            [datetime(2014, 1, 1), datetime(2019, 5, 30)]
        ],
        dict_benchmark_data_specifications={
            'string_name_of_column_with_benchmark_primary_key': 'id',  # Will be id after processing. Columns will be renamed.
            'string_benchmark_key': 'TP3B-248N-Q',  # Ether: T22F-QJGB-N, Bitcoin: TP3B-248N-Q
            'string_file_path_with_benchmark_data': '/home/janspoerer/code/janspoerer/quantbacktest/quantbacktest/assets/raw_itsa_data/20190717_itsa_tokenbase_top600_wtd302_token_daily.csv',
            'float_risk_free_rate': 0.02
        },
        dict_display_options=dict_display_options,
        dict_constraints=dict_constraints,
        dict_general_settings=dict_general_settings,
        dict_comments=dict_comments,
    )

if __name__ == '__main__':
    test_functional_2()
